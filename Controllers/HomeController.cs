﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WeatherForecastFrontend.Models;
using WeatherForecastFrontend.Models.BackendApi;
using WeatherForecastFrontend.Services;

namespace WeatherForecastFrontend.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IUploadService _backendService;
        public HomeController(ILogger<HomeController> logger,
            IUploadService backendService)
        {
            _logger = logger;
            _backendService = backendService;
        }

        public IActionResult Upload()
        {
            GetWeatherListResponseModel weatherListResponse = new GetWeatherListResponseModel();
            return View(weatherListResponse);
        }

        [HttpPost]
        public IActionResult Upload(IFormFile postedFile)
        {
            GetWeatherListResponseModel weatherListResponse = new GetWeatherListResponseModel();
            if (postedFile != null)
            {
                weatherListResponse = _backendService.GetWeatherList(postedFile);
                return View(weatherListResponse);
            }

            return View(weatherListResponse);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
