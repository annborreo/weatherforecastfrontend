﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeatherForecastFrontend.Models;

namespace WeatherForecastFrontend.Mappers
{
    public sealed class CsvWeatherInfoMapper : ClassMap<CsvWeatherInfoModel>
    {
        public CsvWeatherInfoMapper()
        {
            Map(m => m.cityName).Name(Constants.CITYNAME);
        }
    }
}
