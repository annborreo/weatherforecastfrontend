﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherForecastFrontend.Models.BackendApi
{
    public class main
    {
        [Display(Name = "Temperature")]
        public float temp { get; set; }

        [Display(Name = "Feels like")]
        public float feels_like { get; set; }

        [Display(Name = "Minimum Temperature")]
        public float temp_min { get; set; }

        [Display(Name = "Maximum Temperature")]
        public float temp_max { get; set; }

        [Display(Name = "Pressure")]
        public float pressure { get; set; }

        [Display(Name = "Humidity")]
        public float humidity { get; set; }
    }
}
