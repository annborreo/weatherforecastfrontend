using System;
using System.Collections.Generic;

namespace WeatherForecastFrontend.Models.BackendApi
{
    public class GetWeatherListResponseModel
    {
        public GetWeatherListResponseModel()
        {
            weatherInfo = new List<GetWeatherInfoResponse>();
        }
        public List<GetWeatherInfoResponse> weatherInfo { get; set; }
    }
}
