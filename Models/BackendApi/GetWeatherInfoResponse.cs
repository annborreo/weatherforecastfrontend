﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeatherForecastFrontend.Models;

namespace WeatherForecastFrontend.Models.BackendApi
{
    public class GetWeatherInfoResponse
    {
        public main main { get; set; }
        public List<weather> weather { get; set; }
        public string name { get; set; }
        public string degreeSymbol { get; set; }
    }
}
