﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherForecastFrontend.Models
{
    public class Constants
    {
        public const string CITYNAME = "City Name";
        public const string BACKENDAPIURL = "BackendApiUrl";
        public const string GETWEATHERLISTURL = "GetWeatherListUrl";
    }
}
