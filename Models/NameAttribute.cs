﻿using System;

namespace WeatherForecastFrontend.Models
{
    internal class NameAttribute : Attribute
    {
        public NameAttribute(string cityName)
        {
            CityName = cityName;
        }

        public string CityName { get; }
    }
}