﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherForecastFrontend.Models
{
    public class CsvWeatherInfoModel
    {
        [Name(Constants.CITYNAME)]
        public string cityName { get; set; }
    }
}
