﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeatherForecastFrontend.Models.BackendApi;

namespace WeatherForecastFrontend.Services
{
    public interface IUploadService
    {
        GetWeatherListResponseModel GetWeatherList(IFormFile postedFile);
    }
}
