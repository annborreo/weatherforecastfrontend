using RestSharp;

namespace WeatherForecastFrontend.Services
{
    public interface IApiService
    {
        object SendJsonRequest(string url, Method method, object body = null);
    }
}