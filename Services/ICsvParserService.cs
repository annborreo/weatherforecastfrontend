﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeatherForecastFrontend.Models;

namespace WeatherForecastFrontend.Services
{
    public interface ICsvParserService
    {
        List<CsvWeatherInfoModel> ReadFileToWeatherInfoModel(string path);
    }
}
