using RestSharp;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Security;

namespace WeatherForecastFrontend.Services
{
    public class ApiService : IApiService
    {
        public ApiService() {
        }
        /// <summary>
        /// This function sends json request to an API. If the method is POST or PUT, the body parameter 
        /// will automatically added to the parameter request.
        /// If the method is GET or DELETE, the body parameter will not be added to the parameter request.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="method"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public object SendJsonRequest(string url, Method method, object body = null) {
            var client = new RestClient(url);
            var request = new RestRequest();
            request.Method = method;
            request.AddHeader("Accept", "application/json");
            if(method == Method.POST || method == Method.PUT)
            {
                request.Parameters.Clear();
                request.RequestFormat = DataFormat.Json;
                request.AddJsonBody(body);
            }
            var response = client.Execute(request);
            var content = response.Content; 
            return content;
        }
    }
}