﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherForecastFrontend.Mappers;
using WeatherForecastFrontend.Models;

namespace WeatherForecastFrontend.Services
{
    public class CsvParserService : ICsvParserService
    {
        /// <summary>
        /// This function reads the uploaded csv file using CSV Helper
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public List<CsvWeatherInfoModel> ReadFileToWeatherInfoModel(string path)
        {
            try
            {
                using (var reader = new StreamReader(path, Encoding.Default))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    csv.Context.RegisterClassMap<CsvWeatherInfoMapper>();
                    var records = csv.GetRecords<CsvWeatherInfoModel>();
                    return records.ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
