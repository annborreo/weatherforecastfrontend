﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using WeatherForecastFrontend.Models;
using WeatherForecastFrontend.Models.BackendApi;

namespace WeatherForecastFrontend.Services
{
    public class UploadService : IUploadService
    {
        private readonly IApiService _apiService;
        private readonly IConfiguration _configuration;
        private IWebHostEnvironment _environment;
        private readonly ICsvParserService _csvParserService;
        public UploadService(IApiService apiService, IConfiguration configuration,
            IWebHostEnvironment environment, ICsvParserService csvParserService)
        {
            _apiService = apiService;
            _configuration = configuration;
            _environment = environment;
            _csvParserService = csvParserService;
        }
        /// <summary>
        /// This function gets the list of weather info based on the uploaded cities from CSV file
        /// </summary>
        /// <param name="postedFile"></param>
        /// <returns></returns>
        public GetWeatherListResponseModel GetWeatherList(IFormFile postedFile)
        {
            //parse csv
            string path = Path.Combine(_environment.WebRootPath, "Uploads");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string fileName = Path.GetFileName(postedFile.FileName);
            string filePath = Path.Combine(path, fileName);

            using (FileStream stream = new FileStream(filePath, FileMode.Create))
            {
                postedFile.CopyTo(stream);
            }

            var weatherInfoList =_csvParserService.ReadFileToWeatherInfoModel(filePath);

            //send request to backend
            string apiUrl = _configuration[Constants.BACKENDAPIURL];
            string getWeatherListUrl = _configuration[Constants.GETWEATHERLISTURL];
            string url = string.Format(apiUrl, getWeatherListUrl);
            string response = (string)_apiService.SendJsonRequest(url, RestSharp.Method.POST, weatherInfoList);
            var weatherListResponse = JsonSerializer.Deserialize<GetWeatherListResponseModel>(response);

            return weatherListResponse;
        }
    }
}
